//
//  ViewController.h
//  FBShareDemo
//
//  Created by Aashish Tyagi on 4/17/16.
//  Copyright © 2016 Aashish Tyagi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)fbShareClick:(id)sender;

@end

