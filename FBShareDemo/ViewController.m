//
//  ViewController.m
//  FBShareDemo
//
//  Created by Aashish Tyagi on 4/17/16.
//  Copyright © 2016 Aashish Tyagi. All rights reserved.
//

#import "ViewController.h"
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface ViewController ()<FBSDKSharingDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"Welcome KD");
    // Do any additional setup after loading the view, typically from a nib.
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)fbShareClick:(id)sender {
    
    [self fbShare];
}

-(void)fbShare{
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentTitle =@"Testing share title";
    content.contentDescription =@"Mind blowing site";
    
    content.contentURL = [NSURL URLWithString:@"http://www.santabanta.com/"];
    content.imageURL = [NSURL URLWithString:@"http://assets.gearlive.com/tvenvy/blogimages/stewiegriffin.jpg"];
    
    FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc] init];
    shareDialog.delegate=self;
    shareDialog.fromViewController = self;
    shareDialog.shareContent = content;
    
    [shareDialog show];
}


#pragma mark - FBSDKSharingDelegate

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"completed share:%@", results);
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    
    NSLog(@"Error is :%@", [error localizedDescription]);
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    NSLog(@"share cancelled");
}



@end
