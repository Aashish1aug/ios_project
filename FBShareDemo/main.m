//
//  main.m
//  FBShareDemo
//
//  Created by Aashish Tyagi on 4/17/16.
//  Copyright © 2016 Aashish Tyagi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
